---
# backup Pagamento Api - v2, com instruções e passos da criação.
O modelo segue o estilo Clean Architecture.
Todo processo de implementação tem como base a interface IVendaService;


---
Estrutura:

- Criar pasta do Projeto/ pasta_com_nome_do_projeto/
    - criar /app/src
    - Em app teremos o:
        - .gitgignore
        - Readme.md

- Dentro de /src, criar a seguinte estrutura:
    - /App.Api
        - /Controllers
            - VendaController.cs
        - Mover a pasta Properties (criada na criação do projeto) pra este local, pois a mesma recebe configurações do projeto.
        - Rodar o projeto nesse local 
    - /App.Application
        - /DTOs
            - VendaDto.cs
            - VendedorDto.cs
            - UpdateVendaDto.cs
        - /Intefaces 
            - IVendaService.cs]
        - App.Application.csproj
        - Criar AppProfile.cs
        - VendaService.cs
    - /App.Domain: Aqui é o coração do app
        - /Exceptions 
            - DomainException.cs
            - ErrorMessage.cs
        - Venda.cs
        - Vendedor.cs
        - VendaStatus.cs
    - /App.Repository
        - /Interfaces 
            - IVendaRepository.cs
        - VendaRepository.cs
        - DataBaseContext.cs
---