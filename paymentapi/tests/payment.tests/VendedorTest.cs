namespace payment.tests
{
    public class VendedorTest
    {
        /**********************************************/
        private readonly Vendedor vendedorEsperado;
        /**********************************************/        

        /**********************************************/
        public VendedorTest()
        {
            vendedorEsperado = new("33323214144", "Joao", "joao@gmail.com", "58957475154");
        }
        /**********************************************/

        /**********************************************/
        public void Criando_Um_Vendedor()
        {
            Vendedor vendedor = new("33323214144", "Joao", "joao@gmail.com", "58957475154");

            vendedor.Should().BeEquivalentTo(vendedorEsperado);
        }
        /**********************************************/

        /**********************************************/
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void LevantandoExceptionParaCpfInvalido(string cpfInvalido)
        {
            Assert.Throws<ArgumentException>(() => new Vendedor(cpfInvalido, "Joao", "joao@gmail.com", "33323214144"))
                .Message.Should().Be(ErrorMessage.errorCpfIsRequerido);             
        }
        /**********************************************/
    }
}