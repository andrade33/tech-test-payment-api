namespace payment.tests
{
    public class VendaTest
    {
        /*************************************************/
        private readonly Venda vendaEsperada;
        private readonly Vendedor vendedorEsperado;
        private readonly List<Item> listaEsperada;
        private readonly DateTime dataVendaEsperada = DateTime.Now;
        /*************************************************/

        /*************************************************/
        public VendaTest()
        {   
            /* CONSTRUINDO UMA VENDA DE TESTE: 
                A criação de uma venda de teste vai impplicar em:
                    - ter um vendedor
                    - ter itens a venda
                    - ter uma venda propriamente dita
            */

            //Argumentos do vendedor: cpf, nome, email, telefone
            vendedorEsperado = new("87878787855", "Joao", "joao@gmail.com", "58957475154");
            
            // A lista tem os argumentos: Nome, quantidade;
            listaEsperada =  new()
            {
                // Um item foi criado com nome e quantidade
                new Item("Relogio", 3) 
            };
            
            // Arguemntos: data da venda, status, vendedor (dados acima), e uma lista (dados acima);
            vendaEsperada = new(dataVendaEsperada, VendaStatus.AguardandoPagamento, vendedorEsperado, listaEsperada);
        }
        /*************************************************/
        
        /*************************************************/
        // Testando a criação da venda
        [Fact]
        public void Criando_Venda_de_Teste()
        {
            Vendedor vendedor = new("87878787855", "Joao", "joao@gmail.com", "58957475154");
            List<Item> itens = new()
            {
                new Item("Relogio", 3) 
            };
            var venda = new Venda(dataVendaEsperada, VendaStatus.AguardandoPagamento, vendedor, itens);

            venda.Should().BeEquivalentTo(vendaEsperada);
        }
        /*************************************************/
    }
}