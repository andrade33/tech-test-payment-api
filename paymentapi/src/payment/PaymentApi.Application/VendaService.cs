﻿/*********************************************************/
using AutoMapper;
using PaymentApi.Application.Dto;
using PaymentApi.Application.Interfaces;
using PaymentApi.Domain;
using PaymentApi.Repository.Interfaces;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Application
/*********************************************************/

/*********************************************************/
{
    public class VendaService : IVendaService
    {
        /*************************************************/
        private readonly IVendaRepository vendaRepository;
        private readonly IMapper autoMapper;
        /*************************************************/

        /*************************************************/
        public VendaService(
            IVendaRepository vendaRepository,
            IMapper autoMapper
            )
        {
            this.vendaRepository = vendaRepository;
            this.autoMapper = autoMapper;
        }
        /*************************************************/

        /*************************************************/
        public VendaDto Create(CriarVendaDto venda)
        {
             Venda criarVenda = autoMapper.Map<Venda>(venda);
             return autoMapper.Map<VendaDto>(vendaRepository.Create(criarVenda));
        }
        /*************************************************/

        /*************************************************/
        public VendaDto GetById(int id)
        {
            return autoMapper.Map<VendaDto>(vendaRepository.GetById(id));
        }
        /*************************************************/

        /*************************************************/
        public VendaDto Update(int id, UpdateVendaDto updateVendaDto)
        {
            Venda venda = vendaRepository.GetById(id);
            venda.UpdateStatus(updateVendaDto.Status);
            return autoMapper.Map<VendaDto>(vendaRepository.Update(venda));
        }
        /*************************************************/
    }
}