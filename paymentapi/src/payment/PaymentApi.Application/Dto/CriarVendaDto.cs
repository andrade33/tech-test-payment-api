﻿/*********************************************************/
using PaymentApi.Domain;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Application.Dto
/*********************************************************/

/*********************************************************/
{
    public class CriarVendaDto
    {
        /*************************************************/
        /// <summary>
        /// Data da Venda
        /// </summary>
        public DateTime? VendaData { get; set; }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// Vendedor que realizou a venda
        /// </summary>
        public CriarVendedorDto? Vendedor { get; set; }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// Itens vendidos
        /// </summary>
        public List<CriarItemDto>? Itens { get; set; }
        /*************************************************/

        /*************************************************/
        public CriarVendaDto(DateTime vendaData, CriarVendedorDto vendedor, List<CriarItemDto> itens)
        {
            VendaData = vendaData;
            Vendedor = vendedor;
            Itens = itens;
        }
        /*************************************************/

        /*************************************************/
        public CriarVendaDto()
        {

        }
        /*************************************************/
    }
}
