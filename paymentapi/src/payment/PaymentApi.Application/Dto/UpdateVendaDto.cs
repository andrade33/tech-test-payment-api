﻿/*********************************************************/
using PaymentApi.Domain;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Application.Dto
/*********************************************************/

/*********************************************************/
{
    public class UpdateVendaDto
    {
        public VendaStatus Status { get; set; }
    }
}
