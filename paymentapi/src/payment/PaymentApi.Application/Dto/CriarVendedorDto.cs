﻿/*********************************************************/
namespace PaymentApi.Application.Dto
/*********************************************************/

/*********************************************************/
{
    public class CriarVendedorDto
    {
        /*************************************************/
        /// <summary>
        /// CPF do vendedor
        /// </summary>
        public string? Cpf { get; set; }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// Nome do vendedor
        /// </summary>
        public string? Nome { get; set; }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// Email do vendedor
        /// </summary>
        public string? Email { get; set; }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// Telefone do vendedor
        /// </summary>
        public string? Telefone { get; set; }
        /*************************************************/

        /*************************************************/
        public CriarVendedorDto(string cpf, string nome, string email, string telefone)
        {
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        /*************************************************/

        /*************************************************/
        public CriarVendedorDto()
        {

        }
        /*************************************************/
    }
}
