﻿/*********************************************************/
namespace PaymentApi.Application.Dto
/*********************************************************/

/*********************************************************/

/*************************************************/
{
    public class VendedorDto
    {
        /*************************************************/
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        /*************************************************/

        /*************************************************/
        public VendedorDto( string cpf, string nome, string email, string telefone)
        {
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        /*************************************************/

        /*************************************************/
        public VendedorDto()
        {

        }
        /*************************************************/
    }
}
