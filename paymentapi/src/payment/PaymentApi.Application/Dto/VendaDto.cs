﻿/*********************************************************/
using PaymentApi.Domain;
using PaymentApi.Domain.Exceptions;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Application.Dto
/*********************************************************/

/*********************************************************/
{
    public class VendaDto
    {
        /*************************************************/
        public int Id { get; set; }
        public DateTime VendaData { get; set; }
        public VendaStatus Status { get; set; }
        public VendedorDto Vendedor { get; set; }
        public List<ItemDto> Itens { get; set; }
        /*************************************************/

        /*************************************************/
        public VendaDto(DateTime vendaData, VendaStatus status, VendedorDto vendedor, List<ItemDto> itens)
        {
            VendaData = vendaData;
            Status = status;
            Vendedor = vendedor;
            Itens = itens;
        }
        /*************************************************/

        /*************************************************/
        public VendaDto() { }
        /*************************************************/
    }
}
