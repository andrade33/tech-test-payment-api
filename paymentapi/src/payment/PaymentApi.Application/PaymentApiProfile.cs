﻿/*******************************************************/
using AutoMapper;
using PaymentApi.Application.Dto;
using PaymentApi.Domain;
/*******************************************************/

/*******************************************************/
namespace PaymentApi.Application
/*******************************************************/
/*******************************************************/
{
    public class PaymentApiProfile : Profile
    {
        /*************************************************/
        public PaymentApiProfile()
        {
            CreateMap<Venda, VendaDto>().ReverseMap();
            CreateMap<Item, ItemDto>().ReverseMap();
            CreateMap<Vendedor, VendedorDto>().ReverseMap();
            CreateMap<CriarVendaDto, Venda>();
            CreateMap<CriarItemDto, Item>();
            CreateMap<CriarVendedorDto, Vendedor>();
        }
        /*************************************************/
    }
}
