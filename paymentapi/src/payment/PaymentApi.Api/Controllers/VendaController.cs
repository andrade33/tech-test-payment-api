/*********************************************************/
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.Application.Dto;
using PaymentApi.Application.Interfaces;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Api.Controllers
/*********************************************************/

/*********************************************************/
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {        

        private readonly IVendaService vendaService;

        /*************************************************/
        public VendaController(IVendaService vendaService)
        {
            this.vendaService = vendaService;
        }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// - Buscar venda pelo id
        /// </summary>
        /// <remarks>   
        /// <h3>Permite obter dados de uma venda usando o id.</h3>     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Uma venda</returns>
        /************/
        [HttpGet("busca-venda-por-id{id}")]
        public IActionResult Get(int id)
        {
            try
            {
	            var venda = vendaService.GetById(id);
                return venda != null
                    ? Ok(venda)
                    : BadRequest ("Venda não cadastrada");
                /*
                if (venda == null) 
                    return NotFound();
	            return Ok(venda) ;
                */
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// - Use esse campo pra inserir uma venda.
        /// </summary>
        /// <remarks>
        ///  <h3> Insira os dados da venda.</h3>        
        /// </remarks>
        /// <param name="venda"></param>
        /// <returns>Nova venda criada com sucesso</returns>
        /***************/
        [HttpPost("cadastrar-venda")]
        public IActionResult Post([FromBody] CriarVendaDto venda)
        {
            try
            {
                var criarVenda = vendaService.Create(venda);
                return Ok(criarVenda);
            }
            catch (AutoMapperMappingException autoMapper)
            {
                return BadRequest(autoMapper.InnerException?.Message);
            }
            catch (Exception ex) 
            {
                return BadRequest(ex.Message);
            } 
        }
        /*************************************************/

        /*************************************************/
        /// <summary>
        /// - Alterar o status de uma venda.
        /// </summary>
        /// <remarks> 
        /// <h3>Permite alterar o status de uma determinada venda.</h3>    
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="updateVendaDto"></param>
        /// <returns>A venda alterada</returns>
        /***************/
        [HttpPut("atualizar-venda{id}")]
        public IActionResult Put(int id, [FromBody] UpdateVendaDto updateVendaDto)
        {
            try
            {
                var updatedVenda = vendaService.Update(id, updateVendaDto);
                return Ok(updatedVenda);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /*************************************************/
    }
}