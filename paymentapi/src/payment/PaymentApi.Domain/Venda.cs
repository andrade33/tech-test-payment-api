﻿/*********************************************************/
using PaymentApi.Domain.Exceptions;
using System.ComponentModel.DataAnnotations;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Domain
/*********************************************************/

/*********************************************************/
{
    public class Venda
    {
        /*************************************************/
        [Key]
        public int Id { get; private set; }
        public DateTime VendaData { get; private set; }
        public VendaStatus Status { get; private set; }
        public Vendedor Vendedor { get; private set; }
        public List<Item> Itens { get; private set; }
        /*************************************************/

        /*************************************************/
        public Venda(DateTime vendaData, VendaStatus status, Vendedor vendedor, List<Item> itens)
        {
            VendaData = vendaData;
            Status = status;
            Vendedor = vendedor;
            Itens = itens;
        }
        /*************************************************/

        /*************************************************/
        public Venda(DateTime vendaData, Vendedor vendedor, List<Item> itens)
        {
            if (vendaData == new DateTime()) {
                throw new DomainException(ErrorMessage.errorDataRequerida);
            }
            if (vendedor is null){
                throw new DomainException(ErrorMessage.errorVendedorRequerido);
            } 
            if (itens is null || !itens.Any()){
                throw new DomainException(ErrorMessage.errorItemVendidoRequerido);
            } 

            VendaData = vendaData;
            Vendedor = vendedor;
            Itens = itens;
        }
        /*************************************************/

        /*************************************************/
        public Venda(){}
        /*************************************************/

        /*************************************************/
        public void UpdateStatus(VendaStatus vendaStatus)
        {
            CheckIfStatusChangeIsValid(vendaStatus);
            Status = vendaStatus;           
        }
        /*************************************************/

        /*************************************************/
        private void CheckIfStatusChangeIsValid(VendaStatus vendaStatus)
        {
            if (Status == VendaStatus.AguardandoPagamento && (vendaStatus == VendaStatus.EnviadoTransportadora || vendaStatus == VendaStatus.Entregue))
                throw new Exception(string.Format(ErrorMessage.errorAguardandoPagamento, vendaStatus));
            if (Status == VendaStatus.PagamentoAprovado && (vendaStatus == VendaStatus.Entregue || vendaStatus == VendaStatus.AguardandoPagamento))
                throw new Exception(string.Format(ErrorMessage.errorPagamentoAprovado, vendaStatus));
            if (Status == VendaStatus.EnviadoTransportadora && (vendaStatus == VendaStatus.AguardandoPagamento || vendaStatus == VendaStatus.PagamentoAprovado || vendaStatus == VendaStatus.Cancelada))
                throw new Exception(string.Format(ErrorMessage.errorEnviadoTransportadora, vendaStatus));
            if (Status == VendaStatus.Entregue && vendaStatus != VendaStatus.Entregue)
                throw new Exception(string.Format(ErrorMessage.errorEntregue, vendaStatus));
            if (Status == VendaStatus.Cancelada && vendaStatus != VendaStatus.Cancelada)
                throw new Exception(string.Format(ErrorMessage.errorCancelada, vendaStatus));
        }
        /*************************************************/
    }
}
