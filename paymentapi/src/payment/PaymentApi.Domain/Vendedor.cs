﻿/*********************************************************/
using PaymentApi.Domain.Exceptions;
using System.ComponentModel.DataAnnotations;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Domain
/*********************************************************/

/*********************************************************/
{

    public class Vendedor
    {
        /*************************************************/
        [Key]
        public int Id { get; private set; }
        public string Cpf { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Telefone { get; private set; }
        /*************************************************/

        /*************************************************/
        public Vendedor(string cpf, string nome, string email, string telefone)
        {
            if (string.IsNullOrEmpty(cpf)) {
                throw new DomainException(ErrorMessage.errorCpfIsRequerido);
            }
            if (string.IsNullOrEmpty(nome)){
                    throw new DomainException(ErrorMessage.errorNomeRequerido);
            }
            if (string.IsNullOrEmpty(email)){
                throw new DomainException(ErrorMessage.errorEmailRequerido);
            } 
            if (string.IsNullOrEmpty(telefone)){
                throw new DomainException(ErrorMessage.errorTelefoneRequerido);
            } 
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        /*************************************************/

        /*************************************************/
        public Vendedor()
        {

        }
        /*************************************************/
    }
}
