﻿/*********************************************************/
namespace PaymentApi.Domain.Exceptions
/*********************************************************/

/*********************************************************/
{
    public static class ErrorMessage
    {
        /*************************************************/
        public static string errorPagamentoAprovado = "Não é permitido atualizar de Pagamento Aprovado para {0}";
        public static string errorEnviadoTransportadora = "Não é permitido atualizar de Enviado para Transportadora para {0}";
        public static string errorAguardandoPagamento = "Não é permitido atualizar de Aguardando Pagamento para {0}";
        public static string errorEntregue = "Não é permitido atualizar de Entregue para {0}";
        public static string errorCancelada = "Não é permitido atualizar de Cancelada para {0}";
        /*************************************************/
        
        /*************************************************/
        public static string errorCpfIsRequerido = "Campo CPF obrigatório!";
        public static string errorNomeRequerido = "Campo Nome do Vendedor é obrigatório!";
        public static string errorEmailRequerido = "O campo Email é obrigatório!";
        public static string errorTelefoneRequerido = "O campo Telefone é obrigatório!";
        /*************************************************/

        /*************************************************/
        public static string errorNomeItemRequerido = "O campo Nome do Item é obrigatório!";
        public static string errorQuantidadeItemRequerido = "O campo Quantidade deve ser maior que zero!";
        /*************************************************/
        
        /*************************************************/
        public static string errorDataRequerida = "O campo Data é obrigatório!";
        public static string errorVendedorRequerido = "Deve possuir um Vendedor!";
        public static string errorItemVendidoRequerido = "Um item deve ser adicionado!";
        /*************************************************/
    }
}
