﻿/*********************************************************/
using PaymentApi.Domain.Exceptions;
using System.ComponentModel.DataAnnotations;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Domain
/*********************************************************/

/*********************************************************/

{
    public class Item
    {
        /*************************************************/
        [Key]
        public int Id { get; private set; }        
        public string Nome { get; private set; }
        public int Quantidade { get; private set; }
        /*************************************************/

        /*************************************************/
        public Item(string nome, int quantidade)
        {
            if (string.IsNullOrEmpty(nome)) {
                throw new DomainException(ErrorMessage.errorNomeItemRequerido);
            }
            if (quantidade <= 0) 
            {
                throw new DomainException(ErrorMessage.errorQuantidadeItemRequerido);
            }
            Nome = nome;
            Quantidade = quantidade;
        }
        /*************************************************/

        /*************************************************/
        public Item()
        {
        }
        /*************************************************/
    }
}
