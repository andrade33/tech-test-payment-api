﻿/*********************************************************/
using Microsoft.EntityFrameworkCore;
using PaymentApi.Domain;
using PaymentApi.Repository.Interfaces;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Repository
/*********************************************************/

/*********************************************************/
{
    public class VendaRepository : IVendaRepository
    {
        private readonly PaymentContext dataBaseContext;
        
        /*************************************************/
        public VendaRepository(PaymentContext dataBaseContext)
        {
            this.dataBaseContext = dataBaseContext;
        }
        /*************************************************/

        /*************************************************/
        public Venda Create(Venda venda)
        {
            var createdSale = dataBaseContext.Add(venda);
            dataBaseContext.SaveChanges();
            return createdSale.Entity;
        }
        /*************************************************/
        
        /*************************************************/
        public Venda GetById(int id)
        {
            return dataBaseContext.Vendas.Include((s) => s.Vendedor)
                .Include((s) => s.Itens).Where(s => s.Id == id).FirstOrDefault();
        }
        /*************************************************/

        /*************************************************/
        public Venda Update(Venda venda)
        {
            var updatedSale = dataBaseContext.Update(venda);
            dataBaseContext.SaveChanges();
            return updatedSale.Entity;
        }
        /*************************************************/
    }
}