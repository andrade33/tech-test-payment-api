﻿/*********************************************************/
using Microsoft.EntityFrameworkCore;
using PaymentApi.Domain;
/*********************************************************/

/*********************************************************/
namespace PaymentApi.Repository
/*********************************************************/

/*********************************************************/
{
    public class PaymentContext : DbContext
    {
        /*************************************************/
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {
        }
        /*************************************************/

        /*************************************************/
        public DbSet<Item> Itens { get; set; } 
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        /*************************************************/        
    }
}
